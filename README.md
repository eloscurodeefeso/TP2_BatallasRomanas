*Informe TP2 AYP2*
------------------------


**Integrantes:**

- Matias Monti
- Alejandro Araneda
- Gustavo Velazquez
- Ivan Dangelo

**Decisiones de disenio:**

- uso de apache commons para la utilizacion de adapter
- uso de RegistroDeLegiones para no crear objetos(unidades) innecesarias(eficiencia)
- al crear una unidad esta se borra junto a las unidades que contiene y las que la contengan del registro por lo que no podran ser usados por otro jugador
- utilizacion de patron delegacion en paquete batalla, delegando la responsabilidad de implementar el objeto asociado
- implementacion de una interfaz UnidadDeEJercito que dicta el comportamiento para soldados sueltos y legiones.
- separacion de los atributos de auxiliar,legionario, centurion mediantes dos clases, una Soldado para 
auxiliares legionarios y centuriones, otra Legion, para los soldados sueltos y otras legiones
- utilizacion de patron singleton en BaseDeLegiones para no permitir instanciar diferentes bases

**Descripcion de cada archivo:**

_Interfaz De Archivos:_
- BaseDeLegiones: almacena los datos de las legiones disponibles para usar en el juego.
- LectorDeLegiones :quienes implementen esta interface pueden leer y devolver los datos de ciertos archivos en forma de RegistroDeLegion
- LectorPCS: implementa LectorDeLegiones y lee archivos separados por ";"
- LectorPCS: implementa LectorDeLegiones y lee archivos separados por ","
- RegistroDeLegion: estructura que contiene los datos de una legion y calcula su costo

_Interfaz batalla:_
- Iniciar: permite iniciar la batalla siempre que los jugadores hayan comprado un ejercito
- Termino: devuelve verdadero si algun ejercito esta fuera de combate
- Ganador: devuelve el ganador de la batalla
- Atacar: el jugador al turno ataca a sus oponentes
- ProximoJugador: resuelve la logica del proximo jugador al ObtenerJugadorAlTurno
- AlgunoEstaFueraDeCombate: corrobora si algun jugador esta fuera de combate

_Ejercito_
- ComprarUnidad: compra una legion de la base de legiones si le alcanza el dinero y lo enlista a su ejercitoEjercito:
- UnidadDeEjercito: interfaz que define el comportamiento tanto de los soldados como el de una legion
- Soldado: administra la logica para los diferentes tipos de soldado(auxiliar,centurion,ejercito) tanto en el ataque,defensa e incremento de daño para el caso del centurion.
- Legion:en esta clase aplicamos el patron de diseño composite, porque una legion puede ser un soldado o una legion,
sobreescribimos los metodos de calcularCapacidadDeAtaque(), calcularIncrementoAlAtaqueTotal(), tomarPosicion(), estaFueraDeCombate(), 
para el total de todas las legiones y  soldados que tenga el ejercito.
- Ejercito: se encarga de la logica al recibir un ataque de otro ejercito enemigo en la batalla, 
reparte el daño entre los soldados sueltos y otras legiones segun el orden establecido en en ser atacado.


**Conclusiones:**

El trabajo practico nos sirvio para practicar los tres patrones visto en clases,como tambien otros temas vistos en la 
primera parte de la materia.Aprendimos a usar herramientas nuevas que no conociamos, como la libreria de org.apache.commons y el patron de diseño Visitor

//para que se lean los acentos y "enies" = archivo > propiedades > codificacion del archivo de texto > UTF-8.